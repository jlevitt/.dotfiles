# Set a shortcut for reloading your tmux config
bind r source-file ~/.tmux.conf

# Binds {{{
# Change the prefix to C-a
set -g prefix C-a
unbind C-b
bind C-a send-prefix

# hjkl pane traversal
bind h select-pane -L
bind j select-pane -D
bind k select-pane -U
bind l select-pane -R

# hsplit
unbind %
bind | split-window -h

# vsplit
unbind '"'
bind - split-window -v

# Select text to copy with 'v' and copy selection to system clipboard
bind-key -t vi-copy 'v' begin-selection
bind-key -t vi-copy 'y' copy-pipe "reattach-to-user-namespace pbcopy"
# }}}

# Options {{{
# Use vi-style key bindings in copy and choice modes
set-window-option -g mode-keys vi

# Mouse support
set-option -g mouse-resize-pane on
set-option -g mouse-select-pane on
set-option -g mouse-select-window on
set-window-option -g mode-mouse on

# Index windows starting from 1
set-option -g base-index 1

# Shorten the wait after escape
set-option -g escape-time 100

# Launch applications from tmux in OS X
if-shell 'test "$(uname)" = "Darwin"' 'source ~/.tmux-osx.conf'

# }}}

